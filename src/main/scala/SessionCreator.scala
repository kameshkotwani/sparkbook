package org.book.ch08
import org.apache.spark.sql.SparkSession

object SessionCreator {
  def apply(className:String="default"): SparkSession = {
    val spark = SparkSession
      .builder
      .config("spark.eventLog.enabled","true")
      .config("spark.eventLog.dir", "hdfs://192.168.56.2:9000/spark-logs")
      .config("spark.dynamicAllocation.enabled","false")
      .config("spark.scheduler.allocation.file","hdfs://192.168.56.2:9000/fairscheduler.xml")
      .config("spark.scheduler.mode","FAIR")
      .master("spark://master:7077")

      .appName(className)
      .getOrCreate()
    spark
  }
}
