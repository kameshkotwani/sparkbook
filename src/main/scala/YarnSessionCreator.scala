package org.book.ch08

import org.apache.spark.sql.{SparkSession}
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
object YarnSessionCreator {
  def apply(className: String = "default"): SparkSession = {
//    val conf = new SparkConf().setMaster("yarn")
//    val  sc= new SparkContext(conf)
    val spark = SparkSession
      .builder
      .config(new SparkConf().setMaster("yarn"))
      .config("spark.yarn.jars","hdfs://master:9000/spark-jars")
//      .config("spark.yarn.stagingDir","hdfs://master:9000/.sparkStaging/")
      .master("yarn")
//      .config("spark.submit.deployMode","cluster")
      .config("spark.files","hdfs://master:9000/user/bigdata/.sparkStaging")
      .appName(className)
      .getOrCreate()

    spark
  }
}
